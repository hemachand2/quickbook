package com.intuit.quickbook.config;


import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

@Configuration
@Data
@ConfigurationProperties(prefix = "product")
public class QuickbookConfiguration {


    private String ids;

    private List<String> uri = new LinkedList<>();

    private String host;

    public Set<String> getProductIds() {
        if (StringUtils.hasText(ids)) {
            return Arrays.stream(ids.split(",")).map((item) -> item.trim()).collect(Collectors.toSet());
        }
        return Set.of();
    }

    public String getProductValidateAPI(String id) {
        try {
            int num = Integer.parseInt(id.substring(2));
            return uri.get(num);
        } catch (Exception e) {
            return null;
        }
    }

    public String getProductsHost() {
        return host;
    }
}
