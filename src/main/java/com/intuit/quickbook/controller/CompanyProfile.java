package com.intuit.quickbook.controller;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Builder
public class CompanyProfile {

    @NotBlank
    private String name;
    @NotBlank
    private String legalName;
    @NotBlank
    private String bAddress;
    @NotBlank
    private String legalAddress;
    @NotBlank
    private String taxIdentifier;
    @Email
    private String email;
    @NotBlank
    private String website;

}
