package com.intuit.quickbook.controller;

import com.intuit.quickbook.dto.UpdateStatus;
import com.intuit.quickbook.exception.IdNotFoundException;
import com.intuit.quickbook.profile.ProfileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/profile")
public class CompanyProfilerController {

    @Autowired
    private ProfileService profileService;

    private static final Logger logger = LoggerFactory.getLogger(CompanyProfilerController.class);

    @PostMapping(value = "/", consumes = "application/json", produces = "application/json")
    public ProfileResponse addCompanyProfile(@RequestBody CompanyProfile profile) {
        logger.info("Received request to create company profile");

        Map<String, UpdateStatus> uId = profileService.createProfileService(profile);

        ProfileResponse response = uId.entrySet().stream().map(
                (entry) -> new ProfileResponse(entry.getKey(), entry.getValue().toString())).findFirst().get();

        logger.info("Completed request to create company profile");

        return response;

    }

    @GetMapping(value = "/status/{id}", consumes = "application/json", produces = "application/json")
    public ProfileResponse getUpdateStatus(@PathVariable String id) throws IdNotFoundException {
        logger.info("Received request to get company profile");

        UpdateStatus status = profileService.getValidationStatus(id);

        return new ProfileResponse(id, status.toString());
    }

    @GetMapping(value = "/", consumes = "application/json", produces = "application/json")
    public CompanyProfile getProfile() throws IdNotFoundException {
        logger.info("Received request to get company profile");

        CompanyProfile profile = profileService.getCompanyProfile();

        return profile;
    }
}
