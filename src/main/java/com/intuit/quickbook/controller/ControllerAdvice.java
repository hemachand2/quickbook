package com.intuit.quickbook.controller;

import com.intuit.quickbook.exception.IdNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

import java.util.LinkedHashMap;
import java.util.Map;

@org.springframework.web.bind.annotation.ControllerAdvice
public class ControllerAdvice {

    @ExceptionHandler(IdNotFoundException.class)
    @ResponseBody
    public ResponseEntity<Object> handleCityNotFoundException(
            IdNotFoundException ex, WebRequest request) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("message", "Id Not Found");

        return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
    }
}
