package com.intuit.quickbook.repository;

import com.intuit.quickbook.entity.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyProfileRepository extends JpaRepository<Profile, Long> {
    Profile findTopByOrderByIdDesc();
}
