package com.intuit.quickbook.dto;

public enum UpdateStatus {
    IN_PROGRESS,
    ACCEPTED,
    REJECTED
}
