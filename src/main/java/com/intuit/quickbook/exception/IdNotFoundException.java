package com.intuit.quickbook.exception;

public class IdNotFoundException extends RuntimeException {

    String message;

    public IdNotFoundException() {
        message = "";
    }

    public IdNotFoundException(String message) {
        super(message);
        this.message = message;
    }
}
