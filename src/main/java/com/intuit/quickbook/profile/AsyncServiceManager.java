package com.intuit.quickbook.profile;

import com.intuit.quickbook.cache.CacheManager;
import com.intuit.quickbook.config.QuickbookConfiguration;
import com.intuit.quickbook.controller.CompanyProfile;
import com.intuit.quickbook.dto.UpdateStatus;
import com.intuit.quickbook.entity.Profile;
import com.intuit.quickbook.exception.IdNotFoundException;
import com.intuit.quickbook.gateway.ProductGateway;
import com.intuit.quickbook.repository.CompanyProfileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import static com.intuit.quickbook.dto.UpdateStatus.IN_PROGRESS;

@Service
public class AsyncServiceManager {

    private static final Logger logger = LoggerFactory.getLogger(AsyncServiceManager.class);

    @Autowired
    private ProductGateway gateway;

    @Autowired
    private QuickbookConfiguration quickbookConfiguration;

    @Autowired
    private CacheManager cacheManager;

    @Autowired
    private CompanyProfileRepository companyProfileRepository;

    @Async("asyncExecutor")
//    @CachePut(cacheNames = {"requestCache"}, key="#jobId")
    public CompletableFuture<Map<String, UpdateStatus>> callProductAPI(String prodId, String uri, CompanyProfile profile) {

//        logger.info("In Product service, start the gateway: {}, {}", prodId, uri);

        CompletableFuture<Map<String, UpdateStatus>> task =  new CompletableFuture<Map<String, UpdateStatus>>();

        try {
            String stat = gateway.validateProductAPI(uri, profile);
            task.complete(Map.of(prodId, UpdateStatus.valueOf(stat)));
            updateCacheAndDb(prodId, profile);
        } catch (InterruptedException e) {
            task.completeExceptionally(e);
        } catch (Exception e) {
            logger.error("Exception occured: {}", e);
            task.completeExceptionally(e);
        }

        return task;

    }

    public synchronized void updateCacheAndDb(String prodId, CompanyProfile profile) {
        String requestId = prodId.split("#")[0];

        List<CompletableFuture<Map<String, UpdateStatus>>> prodsStatus = null;
        try {
            prodsStatus = cacheManager.getCacheElement(requestId);
        } catch (IdNotFoundException e) {
            logger.error("No Id match found: {}", requestId);
        }

        prodsStatus.stream().forEach(ele -> logger.info("ele ->> : {}", ele.toString()));

        boolean completionStatus = prodsStatus.stream().filter(ele -> ele.isDone()).count() == (long) prodsStatus.size() - 1;

        logger.info("Task ID status with prodid: {}, {}", prodId, completionStatus);

        if (completionStatus) {
            logger.info("Task ID has responses from all products: {}", requestId);
            companyProfileRepository.save(mapCompanyToProfile(profile));
//            cacheManager.deleteCacheElement(requestId);
        } else {
            logger.info("Task ID status are pending id: {}", requestId);
        }
    }


    private Profile mapCompanyToProfile(CompanyProfile profile) {
        return Profile.builder()
                .companyName(profile.getName())
                .legalName(profile.getLegalName())
                .bAddress(profile.getBAddress())
                .lAddress(profile.getLegalAddress())
                .taxIdentity(profile.getTaxIdentifier())
                .email(profile.getEmail())
                .website(profile.getWebsite())
                .build();
    }
}
