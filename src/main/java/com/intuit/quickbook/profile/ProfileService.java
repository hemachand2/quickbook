package com.intuit.quickbook.profile;

import com.intuit.quickbook.cache.CacheManager;
import com.intuit.quickbook.config.QuickbookConfiguration;
import com.intuit.quickbook.controller.CompanyProfile;
import com.intuit.quickbook.dto.UpdateStatus;
import com.intuit.quickbook.entity.Profile;
import com.intuit.quickbook.exception.IdNotFoundException;
import com.intuit.quickbook.repository.CompanyProfileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static com.intuit.quickbook.dto.UpdateStatus.*;

@Service
public class ProfileService {

    @Autowired
    private CompanyProfileRepository repository;

    @Autowired
    private AsyncServiceManager asyncServiceManager;

    @Autowired
    private QuickbookConfiguration quickbookConfiguration;

    @Autowired
    private CacheManager cacheManager;

    private static final Logger logger = LoggerFactory.getLogger(ProfileService.class);

    public Map<String, UpdateStatus> createProfileService(CompanyProfile profile) {

        // Generate the random ID for the request
        String randId = UUID.randomUUID().toString();

        logger.info("Generated randomId: {}", randId);

        if (!StringUtils.hasText(randId)) {
            logger.info("Problem generating Unique ID:");
        }

        UpdateStatus status = createTaskForApproval(randId, profile);

        return Map.of(randId, status);
    }

    public UpdateStatus createTaskForApproval(String id, CompanyProfile profile) {
        String ids = quickbookConfiguration.getIds();

        Set<String> productIds = Set.of();
        if (StringUtils.hasText(ids)) {
            productIds =  Arrays.stream(ids.split(",")).map((item) -> item.trim()).collect(Collectors.toSet());
        }

//        logger.info("Products: {}: {}", productIds, ids);

        for (String productId: productIds) {
            String uri = quickbookConfiguration.getProductValidateAPI(productId);
            logger.info("Triggering Product API validate: {} and uri: {}", productId, uri);

            CompletableFuture<Map<String, UpdateStatus>> triggerStatus =  asyncServiceManager.callProductAPI(id + "#" + productId, uri, profile);

            cacheManager.addCache(id, triggerStatus);
        }

        return IN_PROGRESS;
    }

    public UpdateStatus getValidationStatus(String id) throws IdNotFoundException {
        List<CompletableFuture<Map<String, UpdateStatus>>> futures = cacheManager.getCacheElement(id, true);

        boolean isCompleted = futures.stream().allMatch(ele -> ele.isDone());

        for (CompletableFuture<Map<String, UpdateStatus>> obj: futures) {
            logger.info("Request Id: {}, and each Product Status", id, obj.isDone());
        }

        if (!isCompleted) {
            return UpdateStatus.IN_PROGRESS;
        }
        else {

            UpdateStatus status = IN_PROGRESS;

            for (CompletableFuture<Map<String, UpdateStatus>> obj: futures) {
                if (obj.isDone()) {
                    try {
                        Map<String, UpdateStatus> prodIdStatus = obj.get();
                        if (Objects.isNull(prodIdStatus)) {
                            throw new IdNotFoundException("Prod Id is not Found");
                        }
                        String prodId = prodIdStatus.keySet().stream().findFirst().get();
                        UpdateStatus prodStatus = prodIdStatus.get(prodId);
                        logger.info("Request Id with: prodId: {}, and each Product Status: {}", prodId, status);
                        if (REJECTED.equals(prodStatus)) {
                            return REJECTED;
                        } else if (IN_PROGRESS.equals(prodStatus)){
                            return IN_PROGRESS;
                        } else {
                            status = ACCEPTED;
                        }
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    } catch (ExecutionException e) {
                        throw new RuntimeException(e);
                    }
                }
            }

            return status;
        }
    }

    public CompanyProfile getCompanyProfile() {
        Profile profile = repository.findTopByOrderByIdDesc();

        return mapProfileToCompanyProfile(profile);
    }

    private CompanyProfile mapProfileToCompanyProfile(Profile profile) {
        return CompanyProfile.builder()
                .name(profile.getCompanyName())
                .legalName(profile.getLegalName())
                .bAddress(profile.getBAddress())
                .legalAddress(profile.getLAddress())
                .taxIdentifier(profile.getTaxIdentity())
                .email(profile.getEmail())
                .website(profile.getWebsite())
                .build();
    }
}
