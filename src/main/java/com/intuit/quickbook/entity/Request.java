package com.intuit.quickbook.entity;

import com.intuit.quickbook.dto.UpdateStatus;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import java.sql.Timestamp;

@Data
@Entity
public class Request {

    @Id
    private String id;
    private String request;
    @Enumerated(EnumType.STRING)
    private UpdateStatus status;
    private Timestamp created;
    private Timestamp updated;

}
