package com.intuit.quickbook.cache;

import com.intuit.quickbook.dto.UpdateStatus;
import com.intuit.quickbook.exception.IdNotFoundException;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public interface CacheManager {
    public void addCache(String id, CompletableFuture<Map<String, UpdateStatus>> future);
    public List<CompletableFuture<Map<String, UpdateStatus>>> getCacheElement(String id) throws IdNotFoundException;
    public List<CompletableFuture<Map<String, UpdateStatus>>> getCacheElement(String id, boolean errorIfNotExists) throws IdNotFoundException;
    public void deleteCacheElement(String id);
}
