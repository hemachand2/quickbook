package com.intuit.quickbook.cache;

import com.intuit.quickbook.dto.UpdateStatus;
import com.intuit.quickbook.exception.IdNotFoundException;
import com.intuit.quickbook.profile.AsyncServiceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Service
public class CacheManagerImpl implements CacheManager {

    private static final Logger logger = LoggerFactory.getLogger(CacheManagerImpl.class);

    private final ConcurrentMap<String, List<CompletableFuture<Map<String, UpdateStatus>>>> cache;

    public CacheManagerImpl() {
        cache = new ConcurrentHashMap<>();
    }

    public void addCache(String id, CompletableFuture<Map<String, UpdateStatus>> future) {
        List<CompletableFuture<Map<String, UpdateStatus>>> futures = new LinkedList<>();

        if (!cache.containsKey(id)) {
            futures.add(future);
            cache.put(id, futures);
        } else {
//            futures.addAll(cache.get(id));
//            futures.add(future);
            cache.get(id).add(future);
        }


        logger.info("Added element to cache: {}: {}", id, cache.get(id));
    }

    public List<CompletableFuture<Map<String, UpdateStatus>>> getCacheElement(String id) throws IdNotFoundException {
        return getCacheElement(id, false);
    }

    public List<CompletableFuture<Map<String, UpdateStatus>>> getCacheElement(String id, boolean errorIfNotExists) throws IdNotFoundException {
        logger.info("Fetch element from cache: {}", id);
        List<CompletableFuture<Map<String, UpdateStatus>>> futureObj = cache.get(id);
        if (Objects.isNull(futureObj)) {
            throw new IdNotFoundException("Invalid ID");
        }
        return futureObj;
    }

    public void deleteCacheElement(String id) {
        logger.info("Delete element from cache: {}", id);
        cache.remove(id);
    }
}
