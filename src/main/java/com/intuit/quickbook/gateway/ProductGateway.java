package com.intuit.quickbook.gateway;

import com.intuit.quickbook.config.QuickbookConfiguration;
import com.intuit.quickbook.controller.CompanyProfile;
import com.intuit.quickbook.dto.UpdateStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

@Service
public class ProductGateway {

    private static Logger log = LoggerFactory.getLogger(ProductGateway.class);

    @Autowired
    private RestTemplate template;

    @Autowired
    private QuickbookConfiguration configuration;
//
//    @Async("asyncExecutor")
//    public CompletableFuture<Map<String, UpdateStatus>> callProductAPI(String jobId, String uri, CompanyProfile profile) {
//
//        log.info("In Product service, start the gateway: {}, {}", jobId, uri);
//        UpdateStatus status = null;
//
//        CompletableFuture<Map<String, UpdateStatus>> task =  new CompletableFuture<Map<String, UpdateStatus>>();
//
//        try {
//            String stat = validateProductAPI(uri, profile);
//            log.info("In Product service, completed the gateway: {}, {}", jobId, uri);
//            status = UpdateStatus.valueOf(stat);
//            task.complete(Map.of(jobId, status));
//        } catch (InterruptedException e) {
////            throw new RuntimeException(e);
//            task.completeExceptionally(e);
//        } catch (Exception e) {
//            log.error("Exception occured: {}", e);
//            task.completeExceptionally(e);
//        }
//
//        return task;
//
//    }

    public String validateProductAPI(String uri, CompanyProfile profile) throws InterruptedException {

        HttpHeaders headers = new org.springframework.http.HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<CompanyProfile> requestEntity = new HttpEntity<>(profile, headers);

        String fullUri = configuration.getProductsHost() + uri;

        // Need to use the try catch for Exception handling
        log.info("Calling API with uri:{}", uri);
        GatewayResponse response = template.postForObject(fullUri, requestEntity, GatewayResponse.class);

//        log.info("Received response API with status:{}", response.toString());

        Thread.sleep(4000L);
        log.info("Completed API request :{}", uri);

        return UpdateStatus.ACCEPTED.toString();
    }
}
