package com.intuit.quickbook.gateway;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class GatewayResponse {

    private String status;
}
